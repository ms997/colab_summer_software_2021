from flask import Flask
from .database import db


def create_app():
    app = Flask(__name__, instance_relative_config=True)
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///./test.db'
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    db.init_app(app)


    from . import user
    from . import address
    app.register_blueprint(user.bp)
    app.register_blueprint(address.bp)


    return app